import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { ToastController,AlertController } from 'ionic-angular';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
data:any;prd:Object= {'frmDate':'','toDate':'','cat':''};
showTbl:boolean;noRslt:boolean;
  constructor(public navCtrl: NavController,public http: Http,public toastCtrl: ToastController,public alertCtrl: AlertController){
    this.showTbl = false;this.noRslt = false;
    this.prd = {'frmDate':'','toDate':'','cat':1};
    // this.data =  {
   //              "GiftCheque": 20000,
   //              "TaotalCash": 20000,
   //              "Total": 20000,
   //              "TotalCard": 20000,
   //              "TotalQuantity": 54,
   //              "daycloseData": [{
   //                  "AmountPaid": "342.0000",
   //                  "CustomerName": "Aswini",
   //                  "OrderNumber": "BCC-4660",
   //                  "ReceiptDate": "1/6/2017 12:48:47 PM",
   //                  "ReceiptNumber": "5207",
   //                  "TotalSale": "342.0000"
   //              }, {
   //                  "AmountPaid": "383.0000",
   //                  "CustomerName": "srikanth",
   //                  "OrderNumber": "BCC-4664",
   //                  "ReceiptDate": "1/6/2017 4:38:35 PM",
   //                  "ReceiptNumber": "5211",
   //                  "TotalSale": "383.0000"
   //              }, {
   //                  "AmountPaid": "504.0000",
   //                  "CustomerName": "Nagarthna",
   //                  "OrderNumber": "BCC-4654",
   //                  "ReceiptDate": "1/5/2017 5:51:53 PM",
   //                  "ReceiptNumber": "5201",
   //                  "TotalSale": "504.0000"
   //              }, {
   //                  "AmountPaid": "572.0000",
   //                  "CustomerName": "Anitha",
   //                  "OrderNumber": "BCC-4655",
   //                  "ReceiptDate": "1/5/2017 6:26:19 PM",
   //                  "ReceiptNumber": "5202",
   //                  "TotalSale": "572.0000"
   //              }, {
   //                  "AmountPaid": "671.0000",
   //                  "CustomerName": "Monika",
   //                  "OrderNumber": "BCC-4663",
   //                  "ReceiptDate": "1/6/2017 4:15:14 PM",
   //                  "ReceiptNumber": "5210",
   //                  "TotalSale": "671.0000"
   //              }, {
   //                  "AmountPaid": "833.0000",
   //                  "CustomerName": "Monika",
   //                  "OrderNumber": "BCC-4658",
   //                  "ReceiptDate": "1/6/2017 11:51:35 AM",
   //                  "ReceiptNumber": "5205",
   //                  "TotalSale": "833.0000"
   //              }, {
   //                  "AmountPaid": "950.0000",
   //                  "CustomerName": "Sri vidya",
   //                  "OrderNumber": "BCC-4657",
   //                  "ReceiptDate": "1/5/2017 8:39:23 PM",
   //                  "ReceiptNumber": "5204",
   //                  "TotalSale": "950.0000"
   //              }, {
   //                  "AmountPaid": "994.0000",
   //                  "CustomerName": "Harish",
   //                  "OrderNumber": "BCC-4653",
   //                  "ReceiptDate": "1/5/2017 10:58:44 AM",
   //                  "ReceiptNumber": "5200",
   //                  "TotalSale": "994.0000"
   //              }, {
   //                  "AmountPaid": "1079.0000",
   //                  "CustomerName": "Kirankumar",
   //                  "OrderNumber": "BCC-4665",
   //                  "ReceiptDate": "1/6/2017 5:05:36 PM",
   //                  "ReceiptNumber": "5212",
   //                  "TotalSale": "1079.0000"
   //              }, {
   //                  "AmountPaid": "1472.0000",
   //                  "CustomerName": "Nagabhusan",
   //                  "OrderNumber": "BCC-4656",
   //                  "ReceiptDate": "1/5/2017 8:11:01 PM",
   //                  "ReceiptNumber": "5203",
   //                  "TotalSale": "1472.0000"
   //              }, {
   //                  "AmountPaid": "1489.0000",
   //                  "CustomerName": "Sunitha",
   //                  "OrderNumber": "BCC-4667",
   //                  "ReceiptDate": "1/6/2017 7:01:21 PM",
   //                  "ReceiptNumber": "5214",
   //                  "TotalSale": "1489.0000"
   //              }, {
   //                  "AmountPaid": "1619.0000",
   //                  "CustomerName": "Kumar",
   //                  "OrderNumber": "BCC-4666",
   //                  "ReceiptDate": "1/6/2017 6:59:43 PM",
   //                  "ReceiptNumber": "5213",
   //                  "TotalSale": "1619.0000"
   //              }, {
   //                  "AmountPaid": "1874.0000",
   //                  "CustomerName": "Venkatesh",
   //                  "OrderNumber": "BCC-4661",
   //                  "ReceiptDate": "1/6/2017 12:51:49 PM",
   //                  "ReceiptNumber": "5208",
   //                  "TotalSale": "1874.0000"
   //              }, {
   //                  "AmountPaid": "2038.0000",
   //                  "CustomerName": "Pavankumar",
   //                  "OrderNumber": "BCC-4662",
   //                  "ReceiptDate": "1/6/2017 2:37:26 PM",
   //                  "ReceiptNumber": "5209",
   //                  "TotalSale": "2038.0000"
   //              }]
   //          };
           
}
     changeDateFormate(d){
          var a = d.split('-');
          return a[1]+'/'+a[2]+'/'+a[0];
        }
        createTost(msg){
          let toast = this.toastCtrl.create({
                    message: msg,
                    duration: 3000,
                      position: 'top'
                  });
                  toast.present();
        }
        showAlert(msg) {
            let alert = this.alertCtrl.create({
              title: msg,
              buttons: ['OK']
            });
            alert.present();
          }

    getList(p){
        if ((p.frmDate != '') && (p.toDate != '')) {
        var url = 'http://test.vsanmart.com/Services/DaySaleService1.svc/DaySalesData?FDate='+this.changeDateFormate(p.frmDate)+'&TDate='+this.changeDateFormate(p.toDate)+'&Category='+p.cat;
     this.http.get(url).map(res => res.json()).subscribe(
            da => {
              console.log(da);
              this.showTbl = true;
              if(da.daycloseData.length > 0){this.data = da;}else{this.data = {};this.showAlert('No data Found'); }
      }, err => {
        this.showAlert('No data Found');
        console.log(err);
      }); } else {
          this.showAlert('All Fields are required');
        }
     }

    }
