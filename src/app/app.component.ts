import { Component } from '@angular/core';
import { Platform ,AlertController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';

import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,private network: Network,public alertCtrl: AlertController) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.network.onDisconnect().subscribe(() => {
          let confirm = this.alertCtrl.create({
          title: 'Internet Connection',
          message: 'Please Check Your Network connection',
          buttons: [{text: 'Cancle', handler: () => {console.log('Disagree clicked'); } },
                    {text: 'Exit', handler: () => {platform.exitApp(); console.log('Agree clicked'); } } ]
          });
          confirm.present();
      });
      this.network.onConnect().subscribe(() => {
            console.log('network connected!');
              //this.alert.dismiss();
      });
    });
  }
}

